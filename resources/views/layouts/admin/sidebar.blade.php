<div class="col-md-3 left_col">
          <div class="left_col scroll-view">
            <div class="navbar nav_title" style="border: 0;">
              <a href="{{url('/')}}" class="site_title"><i class="fa fa-paw"></i> <span>Bwfurn</span></a>
            </div>

            <div class="clearfix"></div>

            <!-- menu profile quick info -->
            <div class="profile clearfix">
              <div class="profile_pic">
                <img src="{{ asset('production/images/img.jpg') }}" alt="..." class="img-circle profile_img">
              </div>
              <div class="profile_info">
                <span>Welcome,
                @if (Auth::guest())
                @role('Admin') {{-- Laravel-permission blade helper --}}
                Admin
                @endrole</span>
                @else
                <h2>{{ Auth::user()->name }}</h2>
                @endif
              </div>
              <div class="clearfix"></div>
            </div>
            <!-- /menu profile quick info -->

            <br />

            <!-- sidebar menu -->
            <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
              <div class="menu_section">
                <h3>General</h3>
                <ul class="nav side-menu">
                  <li><a href="{{route('products.index')}}"><i class="fa fa-product-hunt"></i> Products</a></li>
                  <li><a href="{{route('categories.index')}}"><i class="fa fa-bars"></i> Categories</a></li>
                  <li><a href="{{route('suppliers.index')}}"><i class="fa fa-truck"></i> Suppliers</a>
                  </li>
                  <li><a href="{{route('inventory.list')}}"><i class="fa fa-list-alt"></i> Inventory</a>
                  </li>
                </ul>
              </div>
              <div class="menu_section">
                <h3>Others</h3>
                <ul class="nav side-menu">
                  <li><a><i class="fa fa-archive"></i> Archive <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li><a href="{{route('products.archive')}}">Products</a></li>
                      <li><a href="{{route('categories.archive')}}">Categories</a></li>
                      <li><a href="{{route('suppliers.archive')}}">Suppliers</a></li>
                    </ul>
                  </li>
                  <li><a href="{{route('logs.index')}}"><i class="fa fa-history"></i> Logs</a>
                  </li>
                  <li><a><i class="fa fa-lock"></i> Administration Settings <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                        
                        <li><a>Admin<span class="fa fa-chevron-down"></span></a>
                          <ul class="nav child_menu">
                            <li><a href="{{route('roles.index')}}">Roles</a>
                            </li>
                            <li><a href="{{route('permissions.index')}}">Permissions</a>
                            </li>
                          </ul>
                        </li>
                        <li><a href="{{route('users.index')}}">List of Admin</a>
                        </li>
                    </ul>
                    <li><a href="{{route('backup.index')}}"><i class="fa fa-edit"></i> Backup And Restore</a>
                  </li>                  
                </ul>
              </div>

            </div>
            <!-- /sidebar menu -->

            <!-- /menu footer buttons -->
            <div class="sidebar-footer hidden-small">
              <a data-toggle="tooltip" data-placement="top" title="Settings">
                <span class="glyphicon glyphicon-cog" aria-hidden="true"></span>
              </a>
              <a data-toggle="tooltip" data-placement="top" title="FullScreen">
                <span class="glyphicon glyphicon-fullscreen" aria-hidden="true"></span>
              </a>
              <a data-toggle="tooltip" data-placement="top" title="Lock">
                <span class="glyphicon glyphicon-eye-close" aria-hidden="true"></span>
              </a>
              <a data-toggle="tooltip" data-placement="top" title="Logout" href="login.html">
                <span class="glyphicon glyphicon-off" aria-hidden="true"></span>
              </a>
            </div>
            <!-- /menu footer buttons -->
          </div>
        </div>