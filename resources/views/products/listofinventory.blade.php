@extends('layouts.admin.app')
@section('content')
  @include('layouts.modal')
        <div class="clearfix"></div>

              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Inventory <small>List of Inventory</small></h2>

                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    <div class="table-responsive">
                        <table class="table">
                            <thead>
                                <th class="col-md-2">Code</th>
                                <th class="col-md-2">Name</th>
                                <th class="col-md-2">Category</th>
                                <th class="col-md-2">Supplier</th>
                                <th class="col-md-2">Description</th>
                                <th class="col-md-2">Quantity</th>
                                <th class="col-md-2">Price</th>
                            </thead>
                        <tbody>
                        @foreach ($products as $product)
                        <tr>
                            <td><a href="{{route('products.show',$product->id)}}">{{ $product->code }}</a></td>
                            <td>{{ $product->name }}</td>
                            <td>{{ $product->category->name }}</td>
                            <td>{{ $product->supplier->name }}</td>
                            <td>{{ $product->description }}</td>
                            <td>{{ $product->quantity }}</td>
                            <td>{{ $product->price }}</td>
                            <td>                 
                        </tr>
                        @endforeach
                        </tbody>
                        </table>
                    </div>
                            
                        
                  </div>
                </div>
              </div>
@endsection