@extends('layouts.admin.app')

@section('content')
<section class="content">
    <h3>Bwfurn Database Backups</h3>
    <div class="row">
        <div class="col-xs-12 clearfix">
            <a id="create-new-backup-button" href="{{ route('backup.create') }}" class="btn btn-primary pull-right"
               style="margin-bottom:2em;"><i
                    class="fa fa-plus"></i> Create New Backup
            </a>
        </div>
        <div class="col-xs-12">
            
            @if (count($backups))

                <table class="table table-striped table-bordered">
                    <thead>
                    <tr>
                        <th>File</th>
                        <th>Size</th>
                        <th>Date</th>
                        <!-- 
                        <th>Age</th>
                        -->
                        <th></th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($backups as $backup)
                        <?php
                            $size = $backup['file_size']/1048576;
                            $totalsize = number_format((float)$size, 2, '.', '');
                        ?>
                        <tr>
                            <td>{{ $backup['file_name'] }}</td>
                            <td>{{ $totalsize }}</td>
                            <td>
                                {{ gmdate("F d, Y h:i:s A ", $backup['last_modified']) }}
                            </td>
                           
                            <td class="text-right">
                                <a class="btn btn-xs btn-default"
                                   href="{{ route('backup.download',$backup['file_name']) }}"><i
                                        class="fa fa-cloud-download"></i> Download</a>
                                <a class="btn btn-xs btn-danger" data-button-type="delete"
                                   href="{{ route('backup.delete',$backup['file_name'] )}}"><i class="fa fa-trash-o"></i>
                                    Delete</a>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            @else
                <div class="well">
                    <h4>There are no backups</h4>
                </div>
            @endif
        </div>
    </div>
</section>
@endsection