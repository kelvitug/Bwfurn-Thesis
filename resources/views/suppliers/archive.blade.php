@extends('layouts.admin.app')
@section('content')
  @include('layouts.modal')
        <div class="clearfix"></div>

              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Archive <small>List of Supplier Archive</small></h2>

                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    <div class="table-responsive">
                      @include('layouts.errors-and-messages')
                        <table class="table" id="suppliersample" >
                            <thead>
                                <th class="col-md-2">Name</th>
                                <th class="col-md-2">Action</th>
                            </thead>
                        <tbody>
                        @foreach ($suppliers as $supplier)
                        <tr>
                            <td>{{ $supplier->name }}</td>
                            <td>
                              <form action="{{ route('suppliers.restore', $supplier->id) }}" method="POST" class="form-horizontal">

                              <input name="_method" type="hidden" value="PUT">
                              {{ csrf_field() }}

                              <div class="btn-group">
                                <button class="btn btn-success btn-sm edit" type="button" data-toggle="modal" data-target="#confirmDelete" data-title="Restore" data-message="Are you sure you want to restore ?"><i class="fa fa-check"></i>Restore</button>
                              </div>
                              </form>
                            </td>
                        </tr>
                        @endforeach
                        </tbody>
                        </table>
                    </div>
                            
                        
                  </div>
                </div>
              </div>
@endsection