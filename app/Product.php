<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Category;
use App\Supplier;
use Illuminate\Database\Eloquent\SoftDeletes;

class Product extends Model
{
	use SoftDeletes;

	protected $dates = ['deleted_at'];

	
    protected $fillable = [
        'code',
        'name', 
        'description', 
        'type',
        'category_id',
        'supplier_id', 
        'image',
        'quantity',
        'price'
    ];

    public function category()
    {
        return $this->belongsTo(Category::class);
    }

    public function supplier()
    {
        return $this->belongsTo(Supplier::class);
    }
}