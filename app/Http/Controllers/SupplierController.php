<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Session;
use App\Supplier;
use App\Logs\Log;
use Carbon\Carbon;
use DB;

class SupplierController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct() {
        $this->middleware(['auth', 'clearance'])->except('index', 'show');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index() {
        $suppliers = Supplier::orderby('id', 'desc')->get();

        return view('suppliers.index', compact('suppliers'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        return view('suppliers.create');
    }

    /**
     * Store a newly created resource in Storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) { 

    //Validating title and body field
        $this->validate($request, [
            'name'=>'required|unique:suppliers|max:100',
            ]);
        // logs
        DB::table('logs')->insert([
            ['name' => 'Add '.$request->get('name').' in Supplier at '.\Carbon\Carbon::now()
            ->format('M d, Y h:i a').'',
             'created_at' =>\Carbon\Carbon::now()->format('Y-m-d'), 
             'updated_at' =>\Carbon\Carbon::now()->format('Y-m-d')
         ]
        ]);


        $supplier = Supplier::create($request->all());

    //Display a successful message upon save
        $request->session()->flash('message', $supplier->name.' Created successful');
        return redirect()->back();


    }

    public function show($id) {
        $supplier = Supplier::findOrFail($id); //Find supplier of id = $id

        return view ('suppliers.show', compact('supplier'));
    }

    public function edit($id) {
        $supplier = Supplier::findOrFail($id);

        return view('suppliers.edit', compact('supplier'));
    }

    public function update(Request $request, $id) {
        $this->validate($request, [
            'name'=>'required|max:100',
        ]);


        // logs
        DB::table('logs')->insert([
            ['name' => 'Update '.$request->get('name').' in Supplier at '.\Carbon\Carbon::now()
            ->format('M d, Y h:i a').'',
             'created_at' =>\Carbon\Carbon::now()->format('Y-m-d'), 
             'updated_at' =>\Carbon\Carbon::now()->format('Y-m-d')
         ]
        ]);


        $supplier = Supplier::findOrFail($id);
        $supplier->name = $request->input('name');
        $supplier->save();

        $request->session()->flash('message', $supplier->name.' Updated successful');
        return redirect()->back();

    }

    public function destroy($id) {
        $supplier = Supplier::findOrFail($id);
        $supplier->delete();
        // logs
        DB::table('logs')->insert([
            ['name' => 'Remove '.$supplier->name.' in Suppliers at '.\Carbon\Carbon::now()->format('M d, Y h:i a').'',
             'created_at' =>\Carbon\Carbon::now()->format('Y-m-d'), 
             'updated_at' =>\Carbon\Carbon::now()->format('Y-m-d')
         ]
        ]);


        request()->session()->flash('message', 'Delete successful');
        return redirect()->route('suppliers.index');
    }


    public function archive()
    {
        $suppliers = Supplier::onlyTrashed()
                    ->get();

        return view ('suppliers.archive',[
            'suppliers' => $suppliers
        ]);
            
    }

    public function rest($id)
    {
        $supplier = Supplier::onlyTrashed()
                ->where('id', $id)
                ->restore();
        $supplier1 = Supplier::findOrfail($id);

        DB::table('logs')->insert([
        ['name' => 'Restore '.$supplier1->name.' in Suppliers at '.\Carbon\Carbon::now()->format('M d, Y h:i a').'',
        'created_at' =>\Carbon\Carbon::now()->format('Y-m-d'), 
        'updated_at' =>\Carbon\Carbon::now()->format('Y-m-d')
        ]
        ]);

        request()->session()->flash('message','Restore successful');
        return redirect()->route('suppliers.archive');
    }
}
