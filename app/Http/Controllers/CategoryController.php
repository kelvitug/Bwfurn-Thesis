<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Session;
use App\Category;
use App\Logs\Log;
use Carbon\Carbon;
use DB;
class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct() {
        $this->middleware(['auth', 'clearance'])->except('index', 'show');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index() {
        $categories = Category::orderby('id', 'desc')->get();

        return view('categories.index', compact('categories'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        return view('categories.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) { 

    //Validating title and body field
        $this->validate($request, [
            'name'=>'required|unique:products|max:100',
            ]);
        // logs
        DB::table('logs')->insert([
            ['name' => 'Add '.$request->get('name').' in Category at '.\Carbon\Carbon::now()
            ->format('M d, Y h:i a').'',
             'created_at' =>\Carbon\Carbon::now()->format('Y-m-d'), 
             'updated_at' =>\Carbon\Carbon::now()->format('Y-m-d')
         ]
        ]);


        $category = Category::create($request->all());

    //Display a successful message upon save
        $request->session()->flash('message', $category->name.' Created successful');
        return redirect()->back();


    }

    public function show($id) {
        $category = Category::findOrFail($id); //Find category of id = $id

        return view ('categories.show', compact('category'));
    }

    public function edit($id) {
        $category = Category::findOrFail($id);

        return view('categories.edit', compact('category'));
    }

    public function update(Request $request, $id) {
        $this->validate($request, [
            'name'=>'required|max:100',
        ]);


        // logs
        DB::table('logs')->insert([
            ['name' => 'Update '.$request->get('name').' in Category at '.\Carbon\Carbon::now()
            ->format('M d, Y h:i a').'',
             'created_at' =>\Carbon\Carbon::now()->format('Y-m-d'), 
             'updated_at' =>\Carbon\Carbon::now()->format('Y-m-d')
         ]
        ]);


        $category = Category::findOrFail($id);
        $category->name = $request->input('name');
        $category->save();

        $request->session()->flash('message', $category->name.' Updated successful');
        return redirect()->back();

    }

    public function destroy($id) {
        $category = Category::findOrFail($id);
        $category->delete();
        // logs
        DB::table('logs')->insert([
            ['name' => 'Remove '.$category->name.' in Categories at '.\Carbon\Carbon::now()->format('M d, Y h:i a').'',
             'created_at' =>\Carbon\Carbon::now()->format('Y-m-d'), 
             'updated_at' =>\Carbon\Carbon::now()->format('Y-m-d')
         ]
        ]);


        request()->session()->flash('message', 'Delete successful');
        return redirect()->route('categories.index');
    }


    public function archive()
    {
        $categories = Category::onlyTrashed()
                    ->get();

        return view ('categories.archive',[
            'categories' => $categories
        ]);
            
    }

    public function rest($id)
    {
        $category = Category::onlyTrashed()
                ->where('id', $id)
                ->restore();
        $category1 = Category::findOrfail($id);

        DB::table('logs')->insert([
        ['name' => 'Restore '.$category1->name.' in Categories at '.\Carbon\Carbon::now()->format('M d, Y h:i a').'',
        'created_at' =>\Carbon\Carbon::now()->format('Y-m-d'), 
        'updated_at' =>\Carbon\Carbon::now()->format('Y-m-d')
        ]
        ]);

        request()->session()->flash('message', 'Restore successful');
        return redirect()->route('categories.archive');
    }

}
