<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Product;
use App\Category;
use App\Supplier;
use Session;
use App\Logs\Log;
use Carbon\Carbon;
use DB;
class ProductController extends Controller {

    public function __construct() {
        $this->middleware(['auth', 'clearance'])->except('index', 'show');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index() 
    {
        $finished_products = Product::where('type','Featured Product')->orderby('id', 'desc')->get();
        $raw_products = Product::where('type','Raw Materials')->orderby('id', 'desc')->get();
        return view('products.index', compact('finished_products','raw_products'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() 
    {   

        $categories = Category::all();
        $suppliers = Supplier::all();
        return view('products.create',[
            'categories'=> $categories,
            'suppliers'=> $suppliers
        ]);
    }

    public function createRaw()
    {
        $categories = Category::all();
        $suppliers = Supplier::all();
        return view('products.createraw',[
            'categories'=> $categories,
            'suppliers'=> $suppliers
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) 
    { 

    //Validating title and body field
        $this->validate($request, [
            'code'=>'required|unique:products|max:100',
            'name'=>'required|unique:products|max:100',
            'description' =>'required',
            'quantity' =>'required',
            'price' =>'required',
            'image' =>'image|mimes:jpeg,bmp,png|max:2000',
            ]);

        DB::table('logs')->insert([
            ['name' => 'Added '.$request->get('name').' in Products at '.\Carbon\Carbon::now()->format('M d, Y h:i a').'',
             'created_at' =>\Carbon\Carbon::now()->format('Y-m-d'), 
             'updated_at' =>\Carbon\Carbon::now()->format('Y-m-d')
         ]
        ]);

        $code = '';
        if ($request->type=="Finished Product"){
            $code = 'FP-0' .$request->id;
        }else{
            $code = 'RM-0' .$request->id;
        }
        $request->merge([
            'code' => $code
        ]);

        $product = Product::create($request->all());
    

    //Display a successful message upon save
        $request->session()->flash('message', $product->name. ' Created successful');
        return redirect()->back();


    }

    public function show($id) 
    {
        $product = Product::findOrFail($id); //Find product of id = $id

        return view ('products.show', compact('product'));
    }

    public function edit($id) 
    {
        $product = Product::with(['category'])->findOrfail($id);
        $categories = Category::all();

        return view('products.edit', compact('product','categories'));
    }

    public function update(Request $request, $id) 
    {
        $this->validate($request, [
            'code'=>'required|max:100',
            'name'=>'required|max:100',
            'description' =>'required',
            'quantity' =>'required',
            'price' =>'required',
            'image' =>'image|mimes:jpeg,bmp,png|max:2000',
            ]);

         DB::table('logs')->insert([
            ['name' => 'Updated '.$request->get('name').' in Products at '.\Carbon\Carbon::now()
            ->format('M d, Y h:i a').'',
             'created_at' =>\Carbon\Carbon::now()->format('Y-m-d'), 
             'updated_at' =>\Carbon\Carbon::now()->format('Y-m-d')
         ]
        ]);

       
        $product = Product::findOrFail($id)->update($request->all());


        $request->session()->flash('message','Updated successful');
        return redirect()->route('products.edit', $id);

    }

    public function destroy($id) {
        $product = Product::findOrFail($id);
        $product->delete();

        DB::table('logs')->insert([
            ['name' => 'Remove '.$product->name.' in Products at '.\Carbon\Carbon::now()->format('M d, Y h:i a').'',
             'created_at' =>\Carbon\Carbon::now()->format('Y-m-d'), 
             'updated_at' =>\Carbon\Carbon::now()->format('Y-m-d')
         ]
        ]);

        request()->session()->flash('message', 'Delete successful');
        return redirect()->route('products.index');

    }

    public function listofinventory()
    {
        $products = Product::with(['category','supplier'])->get();
        return view ('products.listofinventory',[
            'products' => $products
        ]);
    }

    public function archive()
    {
        $products = Product::onlyTrashed()
                    ->get();

        return view ('products.archive',[
            'products' => $products
        ]);
            
    }

    public function rest($id)
    {
        $product = Product::onlyTrashed()
                ->where('id', $id)
                ->restore();
        $product1 = Product::findOrfail($id);

        DB::table('logs')->insert([
        ['name' => 'Restore '.$product1->name.' in Products at '.\Carbon\Carbon::now()->format('M d, Y h:i a').'',
        'created_at' =>\Carbon\Carbon::now()->format('Y-m-d'), 
        'updated_at' =>\Carbon\Carbon::now()->format('Y-m-d')
        ]
        ]);

        request()->session()->flash('message', 'Restore successful');
        return redirect()->route('products.archive');
    }

}
