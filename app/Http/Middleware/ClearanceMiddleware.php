<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class ClearanceMiddleware 
{
    
    public function handle($request, Closure $next) 
    {        
        if (Auth::user()->hasPermissionTo('Administer roles & permissions')) //If user has this //permission
        {
            return $next($request);
        }
        // product
        if ($request->is('products/create'))//If user is creating a product
         {
            if (!Auth::user()->hasPermissionTo('Create Product'))
         {
                abort('401');
            } 
         else {
                return $next($request);
            }
        }
        // create raw materials
        if ($request->is('products/raw/create'))//If user is creating a product
         {
            if (!Auth::user()->hasPermissionTo('Create Product'))
         {
                abort('401');
            } 
         else {
                return $next($request);
            }
        }


        if ($request->is('products/*/edit')) //If user is editing a product
         {
            if (!Auth::user()->hasPermissionTo('Edit Product')) {
                abort('401');
            } else {
                return $next($request);
            }
        }

        if ($request->isMethod('Delete')) //If user is deleting a product
         {
            if (!Auth::user()->hasPermissionTo('Delete Product')) {
                abort('401');
            } 
         else 
         {
                return $next($request);
            }
        }
        // category
        if ($request->is('categories/create'))//If user is creating a category
         {
            if (!Auth::user()->hasPermissionTo('Create Category'))
         {
                abort('401');
            } 
         else {
                return $next($request);
            }
        }

        if ($request->is('categories/*/edit')) //If user is editing a category
         {
            if (!Auth::user()->hasPermissionTo('Edit Category')) {
                abort('401');
            } else {
                return $next($request);
            }
        }

        if ($request->isMethod('Delete')) //If user is deleting a category
         {
            if (!Auth::user()->hasPermissionTo('Delete Category')) {
                abort('401');
            } 
         else 
         {
                return $next($request);
            }
        }

        // supplier
        if ($request->is('suppliers/create'))//If user is creating a category
         {
            if (!Auth::user()->hasPermissionTo('Create Supplier'))
         {
                abort('401');
            } 
         else {
                return $next($request);
            }
        }

        if ($request->is('suppliers/*/edit')) //If user is editing a category
         {
            if (!Auth::user()->hasPermissionTo('Edit Supplier')) {
                abort('401');
            } else {
                return $next($request);
            }
        }

        if ($request->isMethod('Delete')) //If user is deleting a category
         {
            if (!Auth::user()->hasPermissionTo('Delete Supplier')) {
                abort('401');
            } 
         else 
         {
                return $next($request);
            }
        }



        return $next($request);
    }
}