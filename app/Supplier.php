<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Product;
use Illuminate\Database\Eloquent\SoftDeletes;

class Supplier extends Model
{
    use SoftDeletes;

	protected $dates = ['deleted_at'];

    protected $fillable = ['name'];

    public function products()
    {
        return $this->hasMany('App\Product');   
    }
}
