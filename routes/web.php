<?php

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/', 'DashboardController@index')->name('home');
// Admin Security
Route::resource('logs', 'LogController');
Route::resource('users', 'UserController');

Route::resource('roles', 'RoleController');

Route::resource('permissions', 'PermissionController');
// Product API
Route::put('product-restore/{id}','ProductController@rest')->name('products.restore');
Route::get('products/inventory','ProductController@listofinventory')->name('inventory.list');
Route::get('products/archive','ProductController@archive')->name('products.archive');
Route::get('products/create/raw','ProductController@createraw')->name('products.raw.create');
Route::resource('products', 'ProductController');
// Category API
Route::put('category-restore/{id}','CategoryController@rest')->name('categories.restore');
Route::get('categories/archive','CategoryController@archive')->name('categories.archive');
Route::resource('categories', 'CategoryController');
// Supplier API
Route::put('supplier-restore/{id}','SupplierController@rest')->name('suppliers.restore');
Route::get('suppliers/archive','SupplierController@archive')->name('suppliers.archive');
Route::resource('suppliers', 'SupplierController');


// Backup and restore
Route::get('backup', 'BackupController@index')->name('backup.index');
Route::get('backup/create', 'BackupController@create')->name('backup.create');
Route::get('backup/download/{file_name}', 'BackupController@download')->name('backup.download');
Route::get('backup/delete/{file_name}', 'BackupController@delete')->name('backup.delete');